#!/bin/sh
if [ $(users) != "root" ]; then
	echo "RUN THIS SCRIPT AS ROOT, PLEASE."
	exit 1
fi
if [$(dir artixinstallscript2.sh) != "artixinstallscript2.sh" ]; then
	echo "PLEASE ENSURE THAT BOTH PHASES OF THE SCRIPT HAVE BEEN DOWNLOADED AND ARE IN THE SAME DIRECTORY."
	exit 1
fi

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# DO NOT EXECUTE SCRIPTS WITHOUT READING THEM FIRST.
# THIS ONE INCLUDED.
# IF YOU DO NOT LIKE ANYTHING THIS SCRIPT DOES, PRESS N AT THE NEXT PROMPT.
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

cat $(pwd)/artixinstallscript.sh | less
echo "This script presumes that you have your root partition mounted in /mnt, and internet is set up. Are both of these the case?"
while :
do
	read prelim
	case $prelim in
		[yY][eE][sS]|[yY])
			echo "Okay, continuing..."
			break
			;;
		[nN][oO]|[nN])
			echo "Ending script. Please ensure both of these things are true before running the script."
			exit 0
			;;
		*)
			echo "Input not understood, please use (Y)es/(N)o."
			;;
	esac
done
echo "What init would you like to use? Be warned, this cannot be changed later."
echo "Options:"
echo "1/openrc"
echo "2/runit"
echo "3/s6"
echo "4/suite66"
echo "5/dinit"
while :
do
	read userInit
	case $userInit in
		[oO][pP][eE][nN][rR][cC]|[1])
			progInit="openrc"
			exportInit="1"
			echo "OpenRC Selected"
			break
			;;
		[rR][uU][nN][iI][tT]|[2])
			progInit="runit"
			exportInit="2"
			echo "Runit Selected"
			break
			;;
		[ss][6]|[3])
			progInit="s6"
			exportInit="3"
			echo "s6 selected."
			break
			;;
		[sS][uU][iI][tT][eE][6][6]|[4])
			progInit="suite66"
			exportInit="4"
			echo "suite66 selected."
			break
			;;
		[dD][iI][nN][iI][tT]|5)
			progInit="dinit"
			exportInit="5"
			echo "dinit selected."
			break
			;;
		*)
			echo "Input not understood, please input your init of choice or it's corresponding number above."
			;;
	esac
done
basestrap -i /mnt base elogind-$progInit
echo "What kernel would you like to use? Be warned, this cannot be changed until after you install."
echo "1/linux"
echo "2/lts"
echo "3/zen"
while :
do
	read userKernel
	case $userKernel in
		[lL][iI][nN][uU][xX]|[1])
			progKernel="linux"
			exportKernel="linux"
			echo "Standard Linux Kernel selected."
			break
			;;
		[lL][tT][sS]|[2])
			progKernel="linux-lts"
			exportKernel="lts"
			echo "LTS Kernel selected."
			break
			;;
		[zZ][eE][nN]|[3])
			progKernel="linux-zen"
			exportKernel="zen"
			echo "linux-zen Kernel selected."
			break
			;;
		*)
			echo "Input not understood, please input the kernel name of your choice or the corresponding number."
			;;
	esac
done
basestrap -i /mnt $progKernel $progKernel-headers linux-firmware
while :
do
	echo "What shell would you like to install?"
	read shellchoice
	echo "Is $shellchoice your selection?"
	read confirmation
	case $confirmation in
		[yY][eE][sS]|[yY])
			echo "Okay, locking in $shellchoice..."
			shellchoice=$(echo $shellchoice | tr [:upper:] [:lower:])
			break
			;;
		[nN][oO]|[nN])
			echo "Okay, please make another selection."
			;;
		[*])
			echo "Input not understood, presuming no..."
			;;
	esac
done
echo "Would you like sudo (1), doas (2), or both (3)?"
while :
do
	read rootTool
	case $rootTool in
		[sS][uU][dD][oO]|[1])
			basestrap /mnt base-devel sudo networkmanager grub neovim $shellchoice
			break
			;;
		[dD][oO][aA][sS]|[2])
			basestrap /mnt base-devel doas networkmanager grub neovim $shellchoice
			break
			;;
		[bB][oO][tT][hH]|[3])
			basestrap /mnt base-devel doas sudo networkmanager grub neovim $shellchoice
			break
			;;
		*)
			echo "Input not understood, please input one of the two choices or it's corresponding number."
			;;
	esac
done
fstabgen -U /mnt > /mnt/etc/fstab
cp ./artixinstallscript2.sh /mnt
echo $exportInit > /mnt/selectedinit
echo $exportKernel > /mnt/selectedkernel
echo $shellchoice > /mnt/shellchoice
echo "Install phase one complete."
echo "From here: artix-chroot /mnt and run artixinstallscript2.sh. DO NOT DELETE THE selectedinit OR selectedkernel OR shellchoice FILES, THEY ARE NECESSARY FOR THE SECOND SCRIPT TO FUNCTION, THANK YOU."
