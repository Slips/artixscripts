#!/bin/sh
if [ $(users) != "root" ]; then
	echo "RUN THIS SCRIPT AS ROOT, PLEASE."
	exit 1
fi

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# DO NOT EXECUTE SCRIPTS WITHOUT READING THEM FIRST.
# THIS ONE INCLUDED.
# IF YOU DO NOT LIKE ANYTHING THIS SCRIPT DOES, PRESS N AT THE NEXT PROMPT.
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

cat /artixinstallscript2.sh | less
inputInit=$(cat /selectedinit)
inputKernel=$(cat /selectedkernel)
progShell=$(cat /shellchoice)
case $inputInit in
	1)
		progInit="openrc"
		break
		;;
	2)
		progInit="runit"
		break
		;;
	3)
		progInit="s6"
		break
		;;
	4)
		progInit="suite66"
		break
		;;
	5)
		progInit="dinit"
		break
		;;
	*)
		echo "FATAL ERROR: Unexpected contents of /selectedinit, bailing..."
		exit 1
		;;
esac
case $inputKernel in
        linux)   
                progKernel="linux"
		nvidiaDriverVer=""
                break
                ;;
        lts)
                progKernel="linux-lts"
		nvidiaDriverVer="-lts"
                break
                ;;
        zen)
                progKernel="linux-zen"
		nvidiaDriverVer=""
                break
                ;;
        *)
                echo "FATAL ERROR: Unexpected contents of /selectedkernel, bailing..."
                exit 1
                ;; 
esac

export EDITOR=nvim

if [ $progKernel = "suite66" ]; then
	66-tree -nCE default
	66-tree -n boot
	66-enable -t boot boot@system
	66-env -t boot -e $EDITOR boot@system
	66-enable -t boot -F boot@system
fi
pacman -S --noconfirm chrony chrony-$progInit
case $progInit in
	openrc)
		rc-update add chrony default
		break
		;;
	runit)
		ln -s /etc/runit/sv/chrony /etc/runit/runsvdir/default
		break
		;;
	s6)
		s6-rc-bundle-update -c /etc/s6/rc/compiled add default chronyd
		break
		;;
	suite66)
		66-enable -t default chrony
		break
		;;
	dinit)
		dinitctl enable chrony
		break
		;;
esac
ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
hwclock --systohc
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
echo "export LANG="en_US.UTF-8"" >> /etc/locale.conf
echo "export LC_COLLATE="C"" >> /etc/locale.conf
if [ $progInit = "openrc" ]; then
	echo "keymap="en"" >> /etc/conf.d/keymaps
	echo "windowkeys="YES"" >> /etc/conf.d/keymaps
	mkdir -p /etc/X11/xorg.conf.d/
	touch /etc/X11/xorg.conf.d/00-keyboard.conf
	echo "Section "InputClass"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
	echo "         Identifier "system-keyboard"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
	echo "         MatchIsKeyboard "on"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
	echo "         Option "XkbLayout" "en"" >> /etc/X11/xorg.conf.d/00-keyboard.conf
	echo "EndSection" >> /etc/X11/xorg.conf.d/00-keyboard.conf
fi
echo "Please input desired hostname:"
read hostname
echo $hostname >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $hostname" >> /etc/hosts
if [ $progInit = openrc ]; then
	echo "hostname="$hostname"" >> /etc/conf.d/hostname
fi
echo "AMD or Intel CPU?"
while :
do
	read cpuin
	case $cpuin in
		[aA][mM][dD])
			echo "AMD selected."
			cpu=amd
			break
			;;
		[iI][nN][tT][eE][lL])
			echo "Intel selected."
			cpu=intel
			break
			;;
		*)
			echo "Input not understood, please input either AMD or Intel."
			;;
	esac
done
pacman -S --noconfirm $cpu-ucode
echo "Would you like to install grub for UEFI(1) or BIOS(2)?"
while :
do
	read grubTarget
	case $grubTarget in
		[uU][eE][fF][iI]|[1])
			echo "UEFI selected."
			pacman -S efibootmgr
			grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=artix
			break
			;;
		[bB][iI][oO][sS]|[2])
			echo "BIOS selected."
			while :
			do
				echo "Please give drive path (i.e /dev/___)"
				read drivePath
				echo "Is $drivePath correct? Yes/No (Case Sensitive):"
				read driveIsCorrect
				if [ $driveIsCorrect = "Yes" ]; then
					echo "Okay, continuing..."
					break
				elif [ $driveIsCorrect = "No" ]; then
					echo "Returning to drive path selection..."
				else
					echo "Input not understood, returning, please type "Yes" or "No" (case sensitive) only."
				fi
			done
			grub-install --recheck $drivePath
			break
			;;
		*)
			echo "Input not understood, please try again:"
			;;
	esac
done
grub-mkconfig -o /boot/grub/grub.cfg
pacman -S --noconfirm networkmanager-$progInit dnsmasq
case $progInit in
        openrc)
                rc-update add NetworkManager default
                break
                ;;
        runit)
                ln -s /etc/runit/sv/NetworkManager /etc/runit/runsvdir/default
                break
                ;;
        s6)
                s6-rc-bundle-update -c /etc/s6/rc/compiled add default NetworkManager
                break
                ;;
        suite66)
                66-enable -t default NetworkManager
                break
                ;;
	dinit)
		dinitctl enbale NetworkManager
		break
		;;
		
esac
pacman -S --noconfirm syslog-ng syslog-ng-$progInit logrotate
echo "Set root password:"
passwd
echo "Set username for non-root user:"
read userName
useradd -m -G wheel -s /bin/$progShell $userName
usermod -a -G video $userName
usermod -a -G audio $userName
echo "Set password for non-root user:"
passwd $userName
if [ $(which sudo) != "sudo not found" ]; then
	echo "Sudo found, editing config..."
	sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
fi

if [ $(which doas) != "doas not found" ]; then
	echo "Doas found, making config..."
	echo "permit persist :wheel" > /etc/doas.conf
fi
pacman -S --noconfirm xorg
echo "Would you like to install nvidia drivers?"
while :
do
	read installNvidia
	case $installNvidia in
		[yY][eE][sS]|[yY])
			pacman -S --noconfirm nvidia$nvidiaDriverVer
			break 
			;;
		[nN][oO]|[nN])
			echo "Okay, continuing..."
			break
			;;
		*)
			echo "Input not understood."
			;;
	esac
done
echo "Primary installations complete. If you want any of the selected tools below, please select one with the number or name below. Don't worry, you'll be brought back to this prompt after each install."
while :
do
	echo "1) Git | Version control software"
	echo "2) artix-archlinux-support | All repositories from Arch Linux. Highly recommended unless you're a purist and hate functioning software."
	echo "3) Sneedacity | A fork of audacity without telemetry."
	echo "4) pow | A lightning fast AUR searcher written in C."
	echo "Enter 'N', 'No'. or 'None' for none."
		read installExtraPKG
	    case $installExtraPKG in
		[gG][iI][tT]|[1])
		    pacman -S git
		    ;;
	    [aA][rR][tT][iI][xX][-][aA][rR][cC][hH][lL][iI][nN][uU][xX][-][sS][uU][pP][oO][rR][tT]|[2])
		    pacman -Syu artix-archlinux-support
		    echo "[multilib]" >> /etc/pacman.conf
		    echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf
		    echo "[community]" >> /etc/pacman.conf
		    echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf
		    echo "[extra]" >> /etc/pacman.conf
		    echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf
		    echo "!! !! !! !! !! !!"
		    echo "Arch repos installed. Running pacman -Syyu."
		    pacman -Syyu
		    ;;
		[nN]|[nN][oO]|[nN][oO][nN][eE])
		    break
		    ;;
	    [sS][nN][eE][eE][dD][aA][cC][iI][tT][yY]|[3])
		    echo "[sneed-arch-repo]" >> /etc/pacman.conf
		    echo "SigLevel = Optional TrustAll" >> /etc/pacman.conf
		    echo "Server = https://repo.swurl.xyz/sneed/arch/x86_64" >> /etc/pacman.conf
		    echo "Sneedacity repo installed, Running pacman -Syyu."
		    pacman -Syyu
		    pacman -S sneedacity-bin
		    ;;
	    [pP][oO][wW]|[4])
		    pacman -S --needed tcc scdoc git
		    git clone https://git.envs.net/Slips/pow
		    cd pow
	            make install
		    cd ../
		    rm -rf pow
		    ;;
	    *)
		    echo "Invalid input."
		    ;;
	    esac
	done
rm /selectedinit
rm /selectedkernel
rm /shellchoice
echo "Install phase two complete. To finish, exit this chroot with the exit command, umount -R /mnt, and reboot!"
